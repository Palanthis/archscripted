-UEFI - Verify UEFI MODE!
efivar -l

-If zapping the drive:
gdisk /dev/sdX (x representing your drive. mine is sda)
x
z
y
y

-If wiping the drive at a block level
blkdiscard /dev/sdX

-BIOS Boot
cfdisk /dev/sdX
[New]
First Sector: Leave this blank
Size in sectors: 1024MiB
Hex Code: 0700
partition name: boot

-UEFI Boot
cgdisk /dev/sdX
[New]
First Sector: Leave this blank
Size in sectors: 1024MiB
Hex Code: EF00
partition name: boot

-Root Partition (both)
[New]
First Sector: Leave this blank
Size in sectors: Leave this blank
Hex Code: Leave this blank (default is Linux)
Enter new partition name: root

Quit

mkfs.fat -F32 /dev/sda1

mkfs.ext4 /dev/sda2

mount /dev/sda2 /mnt

mkdir /mnt/boot

mkdir /mnt/boot/efi

mount /dev/sda1 /mnt/boot/efi

mkdir /mnt/home

mount /dev/sdb1 /mnt/home
